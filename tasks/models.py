from django.db import models
from django.conf import settings
from projects.models import Project

# Create your models here.

USER_MODEL = settings.AUTH_USER_MODEL


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        USER_MODEL, on_delete=models.SET_NULL, related_name="tasks", null=True
    )

    def __str__(self):
        return self.name
